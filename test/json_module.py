import pytest
from simpy.io.json import JSON
from simpy.io.packet import PacketUTF8


@pytest.fixture()
def peer(env, _, link_type):
    server = link_type.server(env, ('127.0.0.1', 0))
    conn = link_type.connection(env, server.address)
    return PacketUTF8(env.run(server.accept())), PacketUTF8(env.run(conn))


def test_custom_type(env, peer):
    class MyType(object):
        def __init__(self, value):
            self.value = value

        @staticmethod
        def encode(obj):
            return obj.value

        @staticmethod
        def decode(data):
            return MyType(data)

    class MyConvertible(object):
        def __init__(self, value):
            self.value = value

        @staticmethod
        def encode(obj):
            return obj.value

        @staticmethod
        def decode(data):
            return MyConvertible(data)

    class MyJSON(JSON):
        types = (MyType,)
        converters = (MyConvertible,)

    class Greeter(object):
        @JSON.Descriptor
        class RpcAccessor(JSON.Accessor):
            @property
            def name(self):
                return self.obj.name

            @property
            def parent(self):
                return self.obj.parent

        @RpcAccessor
        def greet(self):
            return MyType('hi there'), MyConvertible('how are you?')

    greeter = Greeter()

    _ = MyJSON(peer[0], greeter.RpcAccessor)
    client = MyJSON(peer[1])

    my_type, my_convertible = yield client.remote.greet()
    assert type(my_type) == MyType
    assert my_type.value == 'hi there'
    assert type(my_convertible) == MyConvertible
    assert my_convertible.value == 'how are you?'
