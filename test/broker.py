import pytest
from simpy.io.json import JSON
from simpy.io.network import RemoteException
from simpy.io.packet import PacketUTF8


# TODO The protocol for node must be configurable, so that the http broker can
# be tested too.


@pytest.fixture()
def peer(env, _, link_type):
    server = link_type.server(env, ('127.0.0.1', 0))
    conn = link_type.connection(env, server.address)
    return PacketUTF8(env.run(server.accept())), PacketUTF8(env.run(conn))


class Node(object):
    @JSON.Descriptor
    class RpcNode(JSON.Accessor):
        @property
        def name(self):
            return self.obj.name

        @property
        def parent(self):
            return self.obj.parent

        def get_child(self, name):
            return self.obj.children[name]

    def __init__(self, name='', *children):
        self.name = name
        self.parent = None
        self.children = {}
        for child in children:
            child.parent = self
            self.children[child.name] = child


def test_broker(env, peer):
    class Greeter(Node):
        @JSON.Descriptor
        class RpcNode(Node.RpcNode):
            pass

        @RpcNode
        def greet(self, name):
            return 'Hi %s, I am a call' % name

        @RpcNode.passthru
        def forward(self, _, request, name):
            request.succeed('Hi %s, I am a passthru call' % name)

        @RpcNode
        def __call__(self, name):
            return 'Hi %s, I am a functor call' % name

        @RpcNode.process
        def process(self, name):
            result = yield env.timeout(0.05, value=name)
            try:
                env.exit('Hi %s, I am a process call' % result)
            except StopIteration as exception:
                return exception.value

    greeter = Greeter()
    _ = JSON(peer[0], greeter.RpcNode)
    client = JSON(peer[1])

    result = yield client.remote.greet('Graham')
    assert result == 'Hi Graham, I am a call'

    result = yield client.remote.forward('John')
    assert result == 'Hi John, I am a passthru call'

    result = yield client.remote('Terry')
    assert result == 'Hi Terry, I am a functor call'

    result = yield client.remote.process('Eric')
    assert result == 'Hi Eric, I am a process call'


def test_expose_process(env, peer):
    class Greeter(Node):
        @JSON.Descriptor
        class RpcNode(Node.RpcNode):
            pass

        @RpcNode.process
        def delayed_greet(self):
            yield env.timeout(0.1)
            try:
                env.exit('hi there')
            except StopIteration as exception:
                return exception.value

    greeter = Greeter()

    _ = JSON(peer[0], greeter.RpcNode)
    client = JSON(peer[1])

    result = yield client.remote.delayed_greet()
    assert result == 'hi there'


def test_access_unexposed_method(env, peer):
    class Greeter(Node):
        def greet(self):
            return 'hi there'

    greeter = Greeter()

    _ = JSON(peer[0], greeter.RpcNode)
    client = JSON(peer[1])

    try:
        yield client.remote.greet()
        pytest.fail('Expected a remote exception')
    except RemoteException as e:
        assert str(e).endswith('RuntimeError: Failed to resolve path greet\n')


def test_access_nonexisting_method(env, peer):
    node = Node()

    _ = JSON(peer[0], node.RpcNode)
    client = JSON(peer[1])

    try:
        yield client.remote.greet()
        pytest.fail('Expected a remote exception')
    except RemoteException as e:
        assert str(e).endswith('RuntimeError: Failed to resolve path greet\n')


def test_callback(env, peer):
    class Publisher(Node):
        @JSON.Descriptor
        class RpcNode(Node.RpcNode):
            pass

        @RpcNode.call
        def set_callback(self, callback):
            self.callback = callback

    class Subscriber(Node):
        @JSON.Descriptor
        class RpcNode(Node.RpcNode):
            pass

        @RpcNode.call
        def callback(self, value):
            self.value = value

    publisher = Publisher()
    subscriber = Subscriber()

    _ = JSON(peer[0], publisher.RpcNode)
    subscriber_rpc = JSON(peer[1], subscriber.RpcNode)

    yield subscriber_rpc.remote.set_callback(subscriber.callback)

    yield publisher.callback('test')
    assert subscriber.value == 'test'


def test_hierarchy(env, peer):
    class CustomNode(Node):
        @JSON.Descriptor
        class RpcNode(Node.RpcNode):
            pass

        @RpcNode
        def get_name(self):
            return self.name

    root = \
        CustomNode(
            'a0',
            CustomNode('a1'),
            CustomNode(
                'b1', CustomNode('a2')
            )
        )

    _ = JSON(peer[0], root.RpcNode)
    peer_1_rpc = JSON(peer[1])

    assert (yield peer_1_rpc.remote.get_name()) == 'a0'
    assert (yield peer_1_rpc.remote.a1.get_name()) == 'a1'
    assert (yield peer_1_rpc.remote.b1.get_name()) == 'b1'
    assert (yield peer_1_rpc.remote.b1.a2.get_name()) == 'a2'


def test_exception(env, peer):
    class FailNode(Node):
        @JSON.Descriptor
        class RpcNode(Node.RpcNode):
            pass

        @RpcNode
        def fail(self):
            raise RuntimeError('I failed')

    failnode = FailNode()

    _ = JSON(peer[0], failnode.RpcNode)
    peer_1_rpc = JSON(peer[1])

    try:
        yield peer_1_rpc.remote.fail()
        assert False, 'Where is the exception?'
    except RemoteException as exc:
        assert exc.remote_traceback.endswith('I failed\n')


def test_result_encoding_exception(env, peer):
    class FailNode(Node):
        @JSON.Descriptor
        class RpcNode(Node.RpcNode):
            pass

        @RpcNode
        def fail(self):
            return object()

    failnode = FailNode()

    _ = JSON(peer[0], failnode.RpcNode)
    peer_1_rpc = JSON(peer[1])

    try:
        yield peer_1_rpc.remote.fail()
        assert False, 'Where is the exception?'
    except RemoteException as exc:
        assert 'ValueError: Failed to encode' in exc.remote_traceback


def test_result_decoding_exception(env, peer):
    class MyType(object):
        def __init__(self, value):
            self.value = value

        @staticmethod
        def encode(obj):
            return obj.value

        @staticmethod
        def decode(data):
            return MyType(data)

    class MyJSON(JSON):
        types = (MyType,)

    class FailNode(Node):
        @JSON.Descriptor
        class RpcNode(Node.RpcNode):
            pass

        @RpcNode
        def fail(self):
            return MyType('eggs and spam')

    failnode = FailNode()

    _ = MyJSON(peer[0], failnode.RpcNode)
    peer_1_rpc = JSON(peer[1])

    # FIXME Should I really need to defuse in this case?
    peer_1_rpc.message.reader.defused = True
    peer_1_rpc.message.writer.defused = True

    try:
        yield peer_1_rpc.remote.fail()
        assert False, 'Where is the exception?'
    except ValueError as exc:
        assert 'Failed to decode' in exc.args[0]


def test_invalid_hierarchy(env, peer):
    class MyNode(Node):
        @JSON.Descriptor
        class RpcNode(Node.RpcNode):
            pass

        def __init__(self):
            Node.__init__(self)
            self.another_root = Node()

        @RpcNode
        def get_wrong_hierarchy(self):
            return self.another_root

    node = MyNode()

    _ = JSON(peer[0], node.RpcNode)
    peer_1_rpc = JSON(peer[1])

    try:
        yield peer_1_rpc.remote.get_wrong_hierarchy()
        assert False, 'Where is the exception?'
    except RemoteException as exc:
        assert 'Failed to encode' in exc.args[1]

# TODO Test exception if calling an object without __call__.
# TODO Test equality and hash function of proxy.
# TODO Test binding a function as root?
# TODO Test broker property of proxies.
# TODO Test connection shutdowns.
# TODO Test forwarding of proxies to a peer with no direct connection to the
# proxies host.
