import pytest

from simpy.io import async_module


@pytest.fixture()
def env(request):
    env = async_module.Environment()
    request.addfinalizer(env.close)
    return env


@pytest.fixture()
def link_type(env, _):
    return async_module.TCPSocket
