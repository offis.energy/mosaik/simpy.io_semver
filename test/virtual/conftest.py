import pytest

from simpy.io import virtual


@pytest.fixture()
def env(_):
    return virtual.Environment()


@pytest.fixture()
def link_type(env, _):
    return virtual.TCPSocket
