from __future__ import absolute_import

import errno
from socket import error as socket_error

import pytest
from simpy.io.codec import JSON
from simpy.io.message import Message, RemoteException, MessageOverflowError
from simpy.io.packet import PacketUTF8
# TODO Test requests with invalid ids.
from simpy.io.platform import connection_errors


@pytest.fixture()
def peer(env, link_type):
    server = link_type.server(env, ('127.0.0.1', 0))
    conn = link_type.connection(env, server.address)
    peer_a = env.run(server.accept())
    peer_b = env.run(conn)
    return (Message(env, PacketUTF8(peer_a), codec=JSON()),
            Message(env, PacketUTF8(peer_b), codec=JSON()))


def test_message(env, link_type):
    server = link_type.server(env, ('127.0.0.1', 0))

    def echo_server(env, name, server):
        service = yield server.accept()
        message = Message(env, PacketUTF8(service), codec=JSON())
        while True:
            request = yield message.recv()
            request.succeed(name + ': ' + request.content)

    env.process(echo_server(env, 'server', server))
    client = Message(
        env,
        PacketUTF8(
            (
                yield link_type.connection(env, server.address)
            )
        ),
        codec=JSON()
    )

    result = yield client.send('hi')
    assert result == 'server: hi'
    result = yield client.send('bye')
    assert result == 'server: bye'


def test_fail_message(env, peer):
    def failing_server(_, peer):
        request = yield peer.recv()
        try:
            raise RuntimeError('I am failing')
        except BaseException as e:
            request.fail(e)

    env.process(failing_server(env, peer[0]))

    try:
        yield peer[1].send('hi')
        assert False, 'Expected an exception'
    except RemoteException as e:
        assert 'raise RuntimeError(\'I am failing\')' in e.remote_traceback


def test_request_trigger_after_close(env, peer):
    requests = []
    for i in range(2):
        peer[0].send('foo')
        requests.append((yield peer[1].recv()))

    peer[1].close()

    # Wait until the transmission processes are terminated.
    try:
        yield peer[1].send('foo')
        assert False, 'Expected an exception'
    except socket_error as e:
        assert e.errno == errno.EBADF

    # Now trigger both requests. Failures are ignored.
    requests[0].succeed()
    try:
        raise RuntimeError('I will be ignored')
    except RuntimeError as e:
        requests[1].fail(e)


def test_incoming_message_limit(env, peer):
    peer[0].message_limit = 5

    for i in range(peer[0].message_limit):
        peer[1].send('foo')

    try:
        yield peer[1].send('spam')
    except ConnectionResetError:
        pass
    except socket_error as e:
        assert e.errno in connection_errors

    assert type(peer[0].reader.value) == MessageOverflowError


def test_outgoing_message_limit(env, peer):
    peer[0].message_limit = 5

    for i in range(peer[0].message_limit):
        peer[0].send('foo')

    try:
        peer[0].send('spam')
    except MessageOverflowError:
        pass


def test_concurrent_send(env, peer):
    requests = [peer[0].send(msg) for msg in ('hi', 'sup?', 'bye')]

    for i in range(3):
        in_request = yield peer[1].recv()
        in_request.succeed('echo: ' + in_request.content)

    results = []
    for request in requests:
        results.append((yield request))

    assert results == ['echo: hi', 'echo: sup?', 'echo: bye']


def test_concurrent_send_reversed(env, peer):
    """Responses may be received in any order."""
    requests = [peer[0].send(msg) for msg in ('hi', 'sup?', 'bye')]

    for i in range(3):
        in_request = yield peer[1].recv()
        in_request.succeed('echo: ' + in_request.content)

    results = []
    for request in reversed(requests):
        results.append((yield request))

    assert results == ['echo: bye', 'echo: sup?', 'echo: hi']


def test_concurrent_recv(env, peer):
    """Concurrent calls to receive are not allowed."""
    peer[1].recv()
    try:
        peer[1].recv()
    except RuntimeError as e:
        assert e.args[0] == 'Concurrent receive attempt'


def test_server_close(env, peer):
    """Any request send is going to be triggered with an error if the
    connection is closed."""
    # FIXME This test is fragile. The request failure depends on whichever IO
    # event is handled first. If the requests are small enough, all data will
    # be successfully written by virtual, causing the reader to fail first.
    # Real network backends fail earlier, presumably because each write call
    # takes some time and allows the close notification to be processed.
    requests = []
    for i in range(10):
        request = peer[0].send('hi' * 1024)
        request.defused = True
        requests.append(request)

    peer[1].close()

    for request in requests:
        try:
            yield request
            assert False, 'Expected an exception'
        except ConnectionError:
            pass
        except socket_error as e:
            assert e.errno in connection_errors


def test_close_process_termination_recv_send(env, peer):
    """Reader and writer processes are not terminated immediately if the
    connection is closed. Only by attempting to read from or write to the
    connection the processes will be terminated."""
    peer_0_request = peer[0].send('hi')
    peer_1_request = yield peer[1].recv()
    peer_1_request.succeed('hi there')
    assert peer_1_request.content == 'hi'
    assert (yield peer_0_request) == 'hi there'

    peer[0].close()
    assert not peer[0].reader.triggered
    assert not peer[0].writer.triggered
    assert not peer[1].reader.triggered
    assert not peer[1].writer.triggered

    try:
        yield peer[0].recv()
        assert False, 'Expected an exception'
    except socket_error as e:
        # TODO Check traceback.
        assert e.errno == errno.EBADF
        assert peer[0].reader.value.errno == errno.EBADF
        assert peer[0].writer.value.errno == errno.EBADF

    try:
        yield peer[0].send('hi')
        assert False, 'Expected an exception'
    except socket_error as e:
        # TODO Check traceback.
        assert e.errno == errno.EBADF

    try:
        yield peer[1].recv()
        assert False, 'Expected an exception'
    except ConnectionError:
        # TODO Check traceback.
        pass
    except socket_error as e:
        assert e.errno in connection_errors

    assert peer[1].reader.value.errno in connection_errors
    assert peer[1].writer.value.errno in connection_errors

    try:
        yield peer[1].send('hi')
        assert False, 'Expected an exception'
    except ConnectionError:
        # TODO Check traceback.
        pass
    except socket_error as e:
        assert e.errno in connection_errors


def test_close_process_termination_send_recv(env, peer):
    """Reader and writer processes are not terminated immediately if the
    connection is closed. Only by attempting to read from or write to the
    connection the processes will be terminated."""
    peer_0_request = peer[0].send('hi')
    peer_1_request = yield peer[1].recv()
    peer_1_request.succeed('hi there')
    assert peer_1_request.content == 'hi'
    assert (yield peer_0_request) == 'hi there'

    peer[0].close()
    assert not peer[0].reader.triggered
    assert not peer[0].writer.triggered
    assert not peer[1].reader.triggered
    assert not peer[1].writer.triggered

    try:
        yield peer[1].send('hi')
        assert False, 'Expected an exception'
    except ConnectionError:
        pass
    except socket_error as e:
        assert e.errno in connection_errors

    assert peer[1].writer.value.errno in connection_errors
    assert peer[1].reader.value.errno in connection_errors

    try:
        yield peer[1].recv()
        assert False, 'Expected an exception'
    except ConnectionError:
        pass
    except socket_error as e:
        assert e.errno in connection_errors

    try:
        yield peer[0].send('hi')
        assert False, 'Expected an exception'
    except socket_error as e:
        assert e.errno == errno.EBADF
        assert peer[0].writer.value.errno == errno.EBADF
        assert peer[0].reader.value.errno == errno.EBADF

    try:
        yield peer[0].recv()
        assert False, 'Expected an exception'
    except socket_error as e:
        assert e.errno == errno.EBADF


def test_sender_close(env, peer):
    """If responses cannot be delivered because the client has closed the
    connection."""
    requests = [peer[0].send('hi') for _ in range(10)]

    peer_1_requests = []
    for i in range(len(requests)):
        peer_1_requests.append((yield peer[1].recv()))

    peer[0].close()

    for request in peer_1_requests:
        request.succeed('hello')

    for request in requests:
        try:
            yield request
            assert False, 'Expected an exception'
        except socket_error as e:
            assert e.errno == errno.EBADF


def test_unconnected_send(env, link_type):
    """Calling send() on an unconnected socket will fail."""
    peer = Message(env, PacketUTF8(link_type(env)), JSON())

    # Send does not fail immediately.
    ev = peer.send('foo')
    try:
        yield ev
    except socket_error as e:
        assert e.errno == errno.ENOTCONN


def test_unconnected_recv(env, link_type):
    """Calling recv() on an unconnected socket will fail."""
    peer = Message(env, PacketUTF8(link_type(env)), JSON())

    try:
        yield peer.recv()
    except socket_error as e:
        assert e.errno == errno.ENOTCONN
