import pytest
from simpy.io import select


@pytest.fixture()
def env(request):
    env = select.Environment()
    request.addfinalizer(env.close)
    return env


@pytest.fixture()
def pipe_type():
    return select.Pipe
