import os

import pytest
from simpy.io import select
from simpy.io.paths import test_dir


class DefaultSSLSocket(select.SSLSocket):
    def __init__(self, env, sock=None, **kwargs):
        key_file_path = os.path.join(str(test_dir), 'secrets', 'server.key.pem')
        kwargs['keyfile'] = key_file_path
        cert_file_path = os.path.join(str(test_dir), 'secrets', 'ca_cert.pem')
        kwargs['certfile'] = cert_file_path
        select.SSLSocket.__init__(self, env, sock, **kwargs)


@pytest.fixture()
def env(request):
    env = select.Environment()
    request.addfinalizer(env.close)
    return env


@pytest.fixture()
def link_type(env):
    return DefaultSSLSocket
