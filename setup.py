# encoding: utf-8
import datetime

from setuptools import setup

TIMESTAMP = str(datetime.datetime.now().replace(microsecond=0).isoformat()).\
    replace('-', '').replace('T', '').replace(':', '')


setup(
    name='SimPy.IO_SemVer',
    version='0.3.0' + 'rc' + TIMESTAMP,
    author='Bengt Lüers',
    author_email='bengt.lueers@gmail.com',
    description='Asynchronous networking based on SimPy.',
    long_description=open('README.rst', 'rb').read().decode('utf-8'),
    url='https://bitbucket.org/simpy/simpy.io',
    license='MIT License',
    install_requires=[
        'SimPy>=3.0.10', 'pytest'
    ],
    packages=['simpy.io'],
    include_package_data=True,
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'Intended Audience :: Education',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: Implementation :: PyPy',
        'Topic :: Scientific/Engineering',
        'Topic :: System :: Networking',
    ],
)
