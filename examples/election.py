from random import seed as set_seed, choice

from simpy.io.json_module import JSON
from simpy.io import async_module
from simpy.io.packet import PacketUTF8


class Node(object):
    @JSON.Descriptor
    class rpc(JSON.Accessor):
        parent = None

    def __init__(self, env, link_type, value):
        self.env = env
        self.link_type = link_type
        self.value = value

        self.server_link = link_type.server(env, ('127.0.0.1', 0))
        self.found_value = 0
        self.initiator = False
        self.engaged = False
        self.count = 0
        self.neighbors = []

        self.env.process(self._serve())

    def _serve(self):
        while True:
            link = yield self.server_link.accept()
            JSON(PacketUTF8(link), self.rpc)

    @rpc
    def explore(self, source, explore_value):
        if not self.engaged:
            self.engaged, self.count, self.source = True, 0, source
            self.found_value = max(self.value, explore_value)
            for neighbor in self.neighbors:
                if neighbor is source: continue
                neighbor.explore(self, self.found_value)

        self.echo(self, self.found_value)

    @rpc
    def echo(self, source, explored_value):
        self.count += 1
        self.found_value = max(self.found_value, explored_value)

        if self.count == len(self.neighbors):
            if self.initiator:
                self.finish()
            else:
                self.source.echo(self, self.found_value)

    @rpc
    def initiate(self):
        self.initiator, self.engaged, self.count = True, True, 0
        for neighbor in self.neighbors:
            neighbor.explore(self, self.value)

        self.done = self.env.event()
        return self.done

    def set_neighbors(self, addresses):
        for address in addresses:
            link = yield self.link_type.connection(self.env, address)
            connection = JSON(PacketUTF8(link), self.rpc)
            self.neighbors.append(connection.remote)

    def finish(self):
        self.done.succeed(self.found_value)


def run_scenario(env, link_type, max_neighbors=3, seed=0):
    set_seed(seed)

    nodes = []
    for i in range(20):
        node = Node(env, link_type, i)
        nodes.append(node)

    # Connect nodes.
    connections = []
    neighborhood = dict((node, []) for node in nodes)
    for node in nodes:
        other = [other for other in nodes if other is not node]
        neighbors = neighborhood[node]
        neighbor_count = choice(range(1, max_neighbors+1))

        while len(neighbors) < neighbor_count:
            other = choice(nodes)
            other_neighbors = neighborhood[other]
            if other is node: continue
            if other in neighbors: continue
            if len(other_neighbors) >= max_neighbors: continue

            neighbors.append(other.server_link.address)
            other_neighbors.append(node.server_link.address)

        yield env.process(node.set_neighbors(neighbors))

    result = yield node.initiate()
    try:
        env.exit(result)
    except StopIteration as exception:
        return exception.args[0]  # == until.value


def main():
    backend = async_module
    env = backend.Environment()
    value = env.run(until=env.process(run_scenario(env, backend.TCPSocket)))
    print('*' * 10, 'Max found value', value)
    return value


if __name__ == '__main__':
    main()
