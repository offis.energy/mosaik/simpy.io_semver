import os
import sys

from simpy.io.async_module import Environment, SSLSocket
from simpy.io.http import HTTP
from simpy.io.websocket import WebSocket

from simpy.io.paths import test_dir

html = """<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <script language='javascript'>

    var socket = new WebSocket('wss://' + document.location.host + '/websock');
    socket.onmessage = function(evt) {
        show_message(evt.data);
    }

    function show_message(text) {
        var message = document.createElement('p');
        message.innerHTML = text;
        var messagelog = document.getElementById('messagelog');
        messagelog.appendChild(message);
        messagelog.scrollTop = messagelog.scrollHeight;
    }

    function submit_message() {
        socket.send(document.message.user.value + ': ' +
                    document.message.text.value);
        document.message.text.value = '';
        return false;
    }
    </script>
</head>
<body>
    <p>Message log</p>
    <div id='messagelog' style='height: 300px; overflow: auto'></div>
    <form name='message' onsubmit='return submit_message();'>
        <input type='text' size='20' name='user' placeholder='Nickname'>
        <input type='text' size='80' name='text' placeholder='Message'>
        <input type='submit'>
    </form>
</body>
</html>
"""


class Server(object):
    @HTTP.Descriptor
    class http(HTTP.Accessor):
        parent = None

    def __init__(self, env, server_sock):
        self.env = env
        self.server_sock = server_sock
        self.sockets = []

        self.env.process(self._serve())

    def _serve(self):
        while True:
            sock = yield self.server_sock.accept()
            HTTP(sock, self.http, client=False)

    @http
    @http.headers({'Content-Type': 'text/html'})
    def __call__(self):
        return html

    @http.passthru
    def websock(self, connection, request):
        excess_data = connection.channel.decommission()
        socket = WebSocket(connection.channel.sock)
        socket.configure(False, headers=request.message.headers)
        self.env.start(self.reader(socket))

    def reader(self, socket):
        self.sockets.append(socket)
        address = socket.peer_address
        yield self.broadcast('New connection from %s' % address)
        try:
            while True:
                message = yield socket.read()
                yield self.broadcast(message)
        except ConnectionResetError:
            self.sockets.remove(socket)
            yield self.broadcast('Connection to %s lost' % address)

    def broadcast(self, msg):
        return self.env.all_of(socket.write(msg) for socket in self.sockets)


if __name__ == '__main__':
    if len(sys.argv) > 1:
        addr = sys.argv[1].split(':', 2)
        addr = (addr[0], 8000) if len(addr) != 2 else (addr[0], int(addr[1]))
    else:
        addr = ('localhost', 8000)

    env = Environment()

    server_sock = SSLSocket(
        env,
        keyfile=os.path.join(str(test_dir), 'server.key'),
        certfile=os.path.join(str(test_dir), 'cacert.pem'),
    )
    server_sock.bind(addr)
    server_sock.listen(5)

    server = Server(env, server_sock)

    env.run()
